package tutorial;

import static tutorial.Utils.print;
import static tutorial.Utils.readDoubleFromConsole;
import static tutorial.Utils.readIntFromConsole;
import static tutorial.Utils.readTextFromConsole;
import static tutorial.Utils.readTrueFalseFromConsole;
import static tutorial.Utils.waitOneSecond;

class TutorialSystem {
    void start() {
        String playerName = chooseName();
        chooseYourWay();
        print("Żegnaj " + playerName);
    }

    private String chooseName() {
        print("Podaj imię");
        String name = readTextFromConsole();
        print("Witaj " + name);
        waitOneSecond();
        return name;
    }

    private void chooseYourWay() {
        print("Czy wybierasz drogę miecza (T/N)");
        while (true) {
            String answer = readTextFromConsole();
            if ("T".equals(answer)) {
                chooseSwordWay();
                break;
            } else if ("N".equals(answer)) {
                chooseNonSwordWay();
                break;
            } else {
                print(prepareErrorMessage());
            }
        }
    }

    private void chooseNonSwordWay() {
        waitOneSecond();
        print("Prośba o wypełnienie formularza.");
        waitOneSecond();
        print("Czy jesteś facetem? (TAK/NIE)");
        boolean isMan = readTrueFalseFromConsole();

        if (!isMan) {
            print("A może jednak pomyśleć o programowaniu pralek? :P");
        }

        print("Podaj wiek.");
        int age = readIntFromConsole();

        if (age < 18) {
            print("Alkohol będziesz mógł kupić za " + (18 - age) + " lat(a)");
        } else {
            print("Alkohol możesz kupić od " + (age - 18) + " lat(a)");
        }

        print("Podaj aktualną cenę żołądkowej gorzkiej");
        double price = readDoubleFromConsole();

        waitOneSecond();
        print("Proszę czekać...");
        waitOneSecond();

        print("Twój wiek: " + age + ", Cena wódki: " + price);

        print("Twoje dane zostały wysłane, chrzanić RODO.");
    }

    private void chooseSwordWay() {
        waitOneSecond();
        print("Podaj słowo do przeliterowania");

        String text = readTextFromConsole();
        char[] chars = text.toCharArray();

        for (int index = chars.length - 1; index >= 0; index--) {
            char aChar = chars[index];
            print(aChar);
            waitOneSecond();
        }
    }

    private String prepareErrorMessage() {
        long currentTimeMillis = System.currentTimeMillis();

        if (currentTimeMillis % 5 == 0) {
            return "BOŻE PRZEROSŁO CIĘ COŚ TAK PROSTEGO SPRÓBÓJ JESZCZE RAZ";
        } else if (currentTimeMillis % 4 == 0) {
            return "BYŁO BLISKO SPRÓBÓJ PONOWNIE";
        } else if (currentTimeMillis % 3 == 0) {
            return "NIE JEST DOBRZE WPISZ JESZCZE RAZ";
        } else {
            return "JESZCZE RAZ :((";
        }
    }
}
