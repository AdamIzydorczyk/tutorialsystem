package tutorial;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

class Utils {
    static void print(String text) {
        System.out.println(text);
    }

    static void print(char text) {
        System.out.println(text);
    }

    static void waitOneSecond() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    static String readTextFromConsole() {
        final Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    static int readIntFromConsole() {
        final Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static boolean readTrueFalseFromConsole() {
        final Scanner scanner = new Scanner(System.in);

        final String nextValue = scanner.next();
        if ("TAK".equals(nextValue)) {
            return true;
        }

        if ("NIE".equals(nextValue)) {
            return false;
        }

        throw new YesNoException();
    }

    static double readDoubleFromConsole() {
        final Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
