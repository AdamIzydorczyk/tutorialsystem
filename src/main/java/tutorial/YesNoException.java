package tutorial;

class YesNoException extends RuntimeException {
    YesNoException() {
        super("Nie wpisano wartości TAK lub NIE, przerywam działanie aplikacji");
    }
}
