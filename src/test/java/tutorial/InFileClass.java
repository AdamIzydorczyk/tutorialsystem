package tutorial;

class InFileClass {
    public final static int CONSTANT_NUMBER = 100;
    private static int staticNumber = 0;
    private int nonStaticNumber = 0;

    public static int getConstantNumberPlus2() {
        return CONSTANT_NUMBER + 2;
    }

    public int getNonStaticNumber() {
        return nonStaticNumber;
    }

    public void setNonStaticNumber(int nonStaticNumber) {
        this.nonStaticNumber = nonStaticNumber;
    }

    public int getStaticNumber() {
        return staticNumber;
    }

    public void setStaticNumber(int num) {
        staticNumber = num;
    }
}
