package tutorial;

import org.junit.Test;

import static java.lang.System.out;

public class JavaKeywords {

    @Test
    public void primitivesNumeric() {
        boolean logicValue = false; // 0 to 1

        byte numberByte = 127; // -127 to 127
        short numberShort = 32767; // 32768 to 32767
        int numberInt = 2147483647; // -2147483 648 to 2147483647
        long numberLong = 9223372036854775807L; // -9223372036854775808 to 9223372036854775807

        float numberFloat = 3.4028235E38F; // -3.4028235E38 to 3.4028235E38
        double numberDouble = 1.7976931348623157E308D; // -1.7976931348623157E308 to 1.7976931348623157E308

        char character = 'B';
    }

    @Test
    public void addOneToMaxInt() {
        int numberInt = 2_147_483_647;
        out.println(numberInt + 1);
        ;
    }

    @Test
    public void intToLong() {
        int numberInt = 2_147_483_647;
        long numberLong;
        numberLong = numberInt;
        out.println(numberLong);
    }

    @Test
    public void longToInt() {
        int numberInt;
        long numberLong = 2_147_483_650L;
        numberInt = (int) numberLong;
        out.println(numberInt);
    }

    @Test
    public void multipleOfFloatAndDouble() {
        out.println(0.1F * 0.1F);
        out.println(0.1D * 0.1D);
        //https://www.youtube.com/watch?v=Zb5P9NneO4g
    }

    @Test
    public void character() {
        char character = 'B';
        char beforeLetter = (char) (character - 1);

        out.println(character);
        out.println((int) character);
        out.println(beforeLetter);
    }

    @Test
    public void finalOnField() {
        final char character = 'B';
        //character = 'C'; compile time error
    }


    @Test
    public void classExample() {
        class Translator {
        }

        Translator firstTranslator = new Translator();
        Translator secondTranslator = new Translator();
    }

    @Test
    public void classWithField() {
        class Translator {
            String errorMessage = "FATAL ERROR OF TRANSLATE";
        }

        Translator translator = new Translator();
        out.println(translator.errorMessage);

        translator.errorMessage = "NEW MESSAGE";
        out.println(translator.errorMessage);
    }

    @Test
    public void classWithFieldTwoInstances() {
        class Translator {
            String errorMessage = "FATAL ERROR OF TRANSLATE";
        }

        Translator firstTranslator = new Translator();
        Translator secondTranslator = new Translator();

        firstTranslator.errorMessage = "NEW MESSAGE";

        out.println(firstTranslator.errorMessage);
        out.println(secondTranslator.errorMessage);
    }

    @Test
    public void methodsAndClass() {
        class Translator {
            String errorMessage = "FATAL ERROR OF TRANSLATE";

            String translate(String textToTranslate) {
                return this.errorMessage + " " + textToTranslate;
            }
        }

        final Translator translator = new Translator();
        out.println(translator.translate("test"));
    }

    @Test
    public void classAndThis() {
        class Translator {
            private String errorMessage = "FATAL ERROR OF TRANSLATE";

            String translate(String textToTranslate) {
                return this.errorMessage + " " + textToTranslate;
            }

            void changeErrorMessage(String errorMessage) {
                // errorMessage = errorMessage;
                this.errorMessage = errorMessage;
            }
        }

        final Translator translator = new Translator();
        translator.changeErrorMessage("NEW MESSAGE");
        translator.translate("TEST");
    }

    @Test
    public void classesLocation() {
        class InMethodClass {
        }

        InMethodClass inMethodClass = new InMethodClass();
        InnerClass innerClass = new InnerClass();
        InFileClass inFileClass = new InFileClass();
    }

    @Test
    public void staticExample() {
        InFileClass first = new InFileClass();
        InFileClass second = new InFileClass();

        first.setNonStaticNumber(1);
        second.setNonStaticNumber(2);

        second.setStaticNumber(10);
        second.setStaticNumber(20);

        out.println(first.getNonStaticNumber());
        out.println(second.getNonStaticNumber());
        out.println(first.getStaticNumber());
        out.println(second.getStaticNumber());
    }

    @Test
    public void staticExampleAgain() {
        int constantNumber = InFileClass.CONSTANT_NUMBER;
        int constantNumberPlus2 = InFileClass.getConstantNumberPlus2();

        out.println(constantNumber);
        out.println(constantNumberPlus2);
    }

    class InnerClass {

    }
}
